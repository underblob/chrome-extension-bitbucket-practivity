# Chrome Extension for Bitbucket Server Pull Request Activity

This extension is published in the [chrome web store](https://chrome.google.com/webstore/detail/ikkojnjgjablomjlcbnejbnfbdifcnee). Check there for details about how it works!
