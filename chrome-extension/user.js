/**
 * @description Expose the current user to the Chrome extension's isolated world.
 * {@link https://developer.chrome.com/extensions/content_scripts#isolated_world}
 * @see scripts.js
 */

require(['bitbucket/util/state'], state => {
  const el = document.querySelector('#chrome-extension-bitbucket-practivity_userjson');
  el.innerHTML = JSON.stringify(state.getCurrentUser());
});
