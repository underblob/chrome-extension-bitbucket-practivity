
const controls = (function() {
  return {
    css() {
      if (this.cssDone) return;
      this.cssDone = true;

      const elStyle = ood.el(`#${EL_ROOT_ID}_style`);
      ood.text(elStyle, `

        ${elStyle.textContent}

        .controls-wrap {
          position: absolute;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
        }
        .controls {
          position: absolute;
          right: 0.5em;
          bottom: 0.4em;
          opacity: 0;
          text-align: right;
          transition: opacity 400ms;
          width: 60px;
          white-space: nowrap;
        }
        .controls-wrap:hover .controls {
          opacity: 1;
        }
        .controls .close,
        .controls .view {
          display: inline-block;
          min-width: 2.0em;
          background: none;
          border: none;
          border-radius: 3px;
          cursor: pointer;
          padding: 0.2em;
          text-align: center;
          transition: box-shadow 600ms;
        }
        .controls .close:hover,
        .controls .view:hover {
          box-shadow: 0 0 3px 1px rgba(0,0,0,0.15);
        }
        .controls .close { color: ${hex.red}; }
        .controls .view { color: ${hex.blue}; }
      `);
    },

    el(activityData, likeData) {
      this.css();

      return ood.make({
        tag: 'div',
        class: 'controls-wrap',
        children: [
          ood.make({
            tag: 'div',
            class: 'controls',
            children: [
              ood.make({
                tag: 'button',
                class: 'view',
                onclick: (e) => this.onViewClick(e, activityData),
                title: 'Inspect',
                children: [
                  ood.make({ tag: 'i', class: 'fas fa-search' })
                ],
              }),
              ood.make({
                tag: 'button',
                class: 'close',
                onclick: (e) => { likeData
                  ? this.onDeleteLikeClick(e, likeData)
                  : this.onDeleteActivityClick(e, activityData);
                },
                title: 'Remove from list',
                children: [
                  ood.make({ tag: 'i', class: 'fas fa-times' }),
                ],
              }),
            ]
          }),
        ],
      });
    },

    onDeleteActivityClick(e, activityData) {
      e.preventDefault();

      const { id } = activityData;
      PR_STORED.activitiesDeleted.push(id);
      ood.del(`#activity-${id}`);

      chrome.storage.sync.set({ [PR_KEY]: PR_STORED});
    },

    onDeleteLikeClick(e, likeData) {
      e.preventDefault();

      const { id } = likeData;
      PR_STORED.likesDeleted.push(id);
      ood.del(`#like-${id}`);

      chrome.storage.sync.set({ [PR_KEY]: PR_STORED });
    },

    onViewClick(e, activityData) {
      e.preventDefault();

      const activityId = _.get(activityData, 'id');
      const commentId = _.get(activityData, 'comment.id');
      const diffPath = _.get(activityData, 'commentAnchor.path');

      let extra;
      if (activityId) extra = `/overview?activityId=${activityId}`;
      if (commentId) extra = `/overview?commentId=${commentId}`;
      if (diffPath) {
        const { line , lineType } = activityData.commentAnchor;
        const dest = lineType === 'ADDED' ? 'T' : 'F';
        const query = (line && `?${dest}${line}`) || '';
        extra = `/diff#${diffPath}${query}`;
      }

      const href = utils.getPath(PATH.PULL_REQUEST, { extra });
      location.href = href;
    },
  };
})();

[
  'onDeleteActivityClick',
  'onDeleteLikeClick',
  'onViewClick',
].forEach(m => controls[m] = controls[m].bind(controls));
