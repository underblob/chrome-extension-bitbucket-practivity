
const likes = (function() {
  return {
    addLikes(data) {
      const elContent = ood.el(`#${EL_ROOT_ID} .content`);

      data.forEach(d => elContent.appendChild(this.el(d)));

      this.css();
    },

    css() {
      if (this.cssDone) return;
      this.cssDone = true;

      const elStyle = ood.el(`#${EL_ROOT_ID}_style`);
      ood.text(elStyle, `

        ${elStyle.textContent}

        .notification .action.liked { background: ${hex.green}; }
      `);
    },

    el(likeData) {
      const { activityData } = likeData;
      return ood.make({
        tag: 'div',
        class: 'notification',
        id: `like-${likeData.id}`,
        children: [
          ood.make({
            tag: 'div',
            class: 'summary-wrap',
            children: [
              ood.make({
                tag: 'p',
                class: 'summary',
                html: `
                  ${likeData.displayName}
                  <b class="action liked">LIKED</b>
                  ${activityData.user.displayName}'s comment`,
              }),
              controls.el(activityData, likeData),
            ],
          }),
        ],
      });
    },
  };
})();
