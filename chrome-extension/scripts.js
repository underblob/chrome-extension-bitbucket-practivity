
let USER;

const scripts = (function() {
  return {

    fontAwesome() {
      return ood.make({
        tag: 'script',
        crossorigin: 'anonymous',
        defer: 'defer',
        integrity: 'sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+',
        src: 'https://use.fontawesome.com/releases/v5.0.10/js/all.js',
      });
    },

    user() {
      return ood.make({
        tag: 'script',
        src: chrome.extension.getURL('/user.js'),
      });
    },

    userJson() {
      return ood.make({
        tag: 'script',
        id: `${EL_ROOT_ID}_userjson`,
        type: 'application/json',
      });
    },

    setUser() {
      const pollUser = setInterval(() => {
        const textJSON = ood.el(`#${EL_ROOT_ID}_userjson`).textContent;
        if (textJSON) {
          clearInterval(pollUser);
          USER = JSON.parse(textJSON);
        }
      }, 3);
    },
  };
})();

document.head.appendChild(scripts.fontAwesome());
document.body.appendChild(scripts.userJson());
document.body.appendChild(scripts.user());
scripts.setUser();
