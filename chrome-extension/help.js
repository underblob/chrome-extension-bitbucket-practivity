
const help = (function() {
  return {
    onDocumentKeyup(e) {
      if (e.key !== '?' || this.isElAppended) return;
      this.isElAppended = true;

      const elTarget = ood.els('#keyboard-shortcut-dialog dt')
        .find(el => el.textContent.includes('Recent repositories'));

      const elTerm = ood.make({ tag: 'dt', text: 'Toggle activities panel' });
      const elDescription = ood.make({
        tag: 'dd',
        children: [ood.make({ tag: 'kbd', text: ']' })],
      });

      elTarget.parentNode.insertBefore(elTerm, elTarget);
      elTarget.parentNode.insertBefore(elDescription, elTarget);
    },
  };
})();

help.onDocumentKeyup = help.onDocumentKeyup.bind(help);
document.addEventListener('keyup', help.onDocumentKeyup);
