
const ONE_SECOND = 1000;
const ONE_HOUR = ONE_SECOND * 60 * 60;
const EXPIRY = ONE_HOUR * 2;
const POLL = ONE_SECOND * 9;

const root = (function() {
  return {
    css() {
      return ood.make({
        tag: 'style',
        id: `${EL_ROOT_ID}_style`,
        type: 'text/css',
        text: `
          #${EL_ROOT_ID} {
            position: fixed;
            bottom: 5px;
            right: 5px;
            width: 30%;
            min-width: 400px;
            max-height: 94vh;
            z-index: 99;
            transition: transform 200ms ease-out;
          }
            #${EL_ROOT_ID} .btn-root {
              position: absolute;
              top: -2.2em;
              width: 2.2em;
              height: 2.0em;
              background: ${hex.blue};
              border-radius: 5px;
              color: #fff;
              cursor: pointer;
              text-align: center;
            }
            #${EL_ROOT_ID} .toggle-close {
              left: 0;
              transition: transform 200ms ease-out;
            }
            #${EL_ROOT_ID} .delete-all {
              right: 0.6em;
              background: ${hex.red};
            }
            #${EL_ROOT_ID} .content {
              background: #fff;
              border-radius: 5px;
              border: solid 1px #ccc;
              box-shadow: 0 0 5px 2px rgba(0,0,0,0.15);
              overflow-x: hidden;
              overflow-y: display;
            }
          #${EL_ROOT_ID}.close {
            transform: translateX(98%);
          }
            #${EL_ROOT_ID}.close .toggle-close {
              transform: translate(-1.4em, 2.6em);
            }
          #${EL_ROOT_ID}.hide, #${EL_ROOT_ID} .hide {
            display: none;
          }
        `,
      });
    },

    el() {
      return ood.make({
        tag: 'div',
        class: 'hide',
        id: EL_ROOT_ID,
        children: [
          ood.make({
            tag: 'button',
            class: 'btn-root toggle-close',
            onclick: e => this.onToggleCloseClick(e),
            title: 'Toggle panel',
            children: [
              ood.make({ tag: 'i', class: 'fas fa-angle-double-right', id: 'toggle-icon-hide' }),
              ood.make({ tag: 'i', class: 'fas fa-angle-double-left hide', id: 'toggle-icon-show' }),
            ],
          }),
          ood.make({
            tag: 'button',
            class: 'btn-root delete-all',
            onclick: e => this.onDeleteAllClick(e),
            title: 'Remove all',
            children: [
              ood.make({ tag: 'i', class: 'fas fa-trash' }),
            ],
          }),
          ood.make({ tag: 'div', class: 'content' }),
        ],
      });
    },

    fetchData() {
      this.fetchActivities()
        .then(activitiesData => {
          const { parsed, promisesLikes } = this.parseActivitiesData(activitiesData);

          const now = new Date().valueOf();
          const activitiesFiltered = parsed
            // Filter activities of the current user.
            .filter(ad => ad.user.id !== USER.id)
            // Filter deleted activities.
            .filter(ad => !PR_STORED.activitiesDeleted.includes(ad.id))
            // Filter expired activities.
            .filter(ad => ad.createdDate > now - EXPIRY);

          Promise.all(promisesLikes)
            .then(likesData => {
              // Flatten likes.
              const likesFlat = [];
              likesData.forEach(l => l.forEach(ll => likesFlat.push(ll)));
              const likesFiltered = likesFlat
                // Filter deleted likes.
                .filter(lf => !PR_STORED.likesDeleted.includes(lf.id));

              const isEmpty = !likesFiltered.length && !activitiesFiltered.length;
              const elRoot = ood.el(`#${EL_ROOT_ID}`);
              elRoot.classList.toggle('hide', isEmpty);

              const elContent = ood.el(`#${EL_ROOT_ID} .content`);
              elContent.innerHTML = '';

              likes.addLikes(likesFiltered);
              activities.addActivities(activitiesFiltered);
            });
        })
    },

    fetchActivities() {
      return fetch(utils.getPath(PATH.API_ACTIVITY), { credentials: 'include' })
        .then(res => res.json())
        .then(json => json.values)
        .catch(err => { throw new Error(err); });
    },

    fetchLikes(activityData) {
      const commentId = (activityData.comment && activityData.comment.id) || activityData.id;
      const apiPath = utils.getPath(PATH.API_LIKES, { commentId });

      return fetch(apiPath, { credentials: 'include' })
        .then(res => res.json())
        .then(json => json.values.map(like => ({ ...like, activityData })))
        .catch(err => { throw new Error(err); });
    },

    onDeleteAllClick(e) {
      const buttons = ood.els('button.close');
      buttons.forEach(b => b.click());

      const elRoot = ood.el(`#${EL_ROOT_ID}`);
      elRoot.classList.add('hide');
},

    onDocumentKeyup(e) {
      const { tagName, type } = document.activeElement;
      const isAbort = (
        (tagName.toLowerCase() === 'input' && type === 'text') ||
        tagName.toLowerCase() === 'textarea' ||
        e.key !== ']'
      );
      if (isAbort) return;

      this.toggleClose();
    },

    onToggleCloseClick(e) {
      this.toggleClose();
    },

    parseActivitiesData(activitiesData = []) {
      const flat = [];
      const promisesLikes = [];

      const flattenComments = (comments, commentAnchor) => {
        while (comments.length) {
          let comment = comments.pop();
          comment.action = 'REPLIED';
          comment.commentAnchor = commentAnchor;
          comment.user = comment.author;

          flat.push(comment);
          promisesLikes.push(this.fetchLikes(comment));

          if (comment.comments.length) {
            flattenComments(comment.comments, commentAnchor);
          }
        }
      };

      activitiesData
        // Filter data into only comments before flatten.
        .filter(ad => ad.comment && !!ad.comment.comments.length)
        .forEach(ad => {
          promisesLikes.push(this.fetchLikes(ad));
          flattenComments(ad.comment.comments, ad.commentAnchor)
        });

      const parsed = _.sortBy(activitiesData.concat(flat), 'createdDate').reverse();

      return { parsed, promisesLikes };
    },

    toggleClose() {
      if (typeof this.isHidden !== 'boolean') this.isHidden = false;
      this.isHidden = !this.isHidden;

      const elRoot = ood.el(`#${EL_ROOT_ID}`);
      elRoot.classList.toggle('close', this.isHidden);
      const elIconHide = ood.el(`#${EL_ROOT_ID} #toggle-icon-hide`);
      elIconHide.classList.toggle('hide', this.isHidden);
      const elIconShow = ood.el(`#${EL_ROOT_ID} #toggle-icon-show`);
      elIconShow.classList.toggle('hide', !this.isHidden);
    },
  };
})();

document.head.appendChild(root.css());
document.body.appendChild(root.el());

[
  'onDeleteAllClick',
  'onDocumentKeyup',
  'onToggleCloseClick',
].forEach(m => root[m] = root[m].bind(root));
document.addEventListener('keyup', root.onDocumentKeyup);

setInterval(() => root.fetchData(), POLL);
