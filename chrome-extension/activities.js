
const activities = (function() {
  return {
    addActivities(activitiesData) {
      const elContent = ood.el(`#${EL_ROOT_ID} .content`);

      activitiesData.forEach(ad => elContent.appendChild(this.el(ad)));

      this.css();
    },

    css() {
      if (this.cssDone) return;
      this.cssDone = true;

      const elStyle = ood.el(`#${EL_ROOT_ID}_style`);
      ood.text(elStyle, `

        ${elStyle.textContent}

        .notification {
          position: relative;
          border-bottom: solid 1px #ccc;
          padding: 0.5em 1.0em;
        }
        .notification .summary-wrap {
          padding: 0 60px 0 0;
        }
        .notification .comment {
          height: 1.5em;
          width: 100%;
          color: #666;
          font-size: 0.8em;
          margin: 0.2em 0 0;
          padding: 0;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
        }
        .notification .action {
          color: #fff;
          background: ${hex.blue};
          border-radius: 3px;
          font-size: 0.75em;
          padding: 0.2em 0.4em;
        }
        .notification .action.approved { background: ${hex.green}; }
        .notification .action.declined { background: ${hex.red}; }
        .notification .action.merged { background: ${hex.green}; }
        .notification .action.rescoped { background: ${hex.yellow}; color: ${hex.brown}; }
        .notification .action.reviewed { background: ${hex.red}; }
        .notification .action.unapproved { background: ${hex.red}; }
      `);
    },

    el(activityData) {
      const { action, comment, createdDate, id, user, text } = activityData;
      const commentId = comment && comment.id;

      return ood.make({
        tag: 'div',
        class: 'notification',
        ['data-info']: JSON.stringify({ activityId: id, commentId }),
        id: `activity-${id}`,
        children: [
          ood.make({
            tag: 'div',
            class: 'summary-wrap',
            children: [
              ood.make({
                tag: 'p',
                class: 'summary',
                html: `
                  ${user.displayName}
                  <b class="action ${action.toLowerCase()}">${action}</b>
                  ${timeago().format(createdDate)}`,
              }),
              (comment && comment.text && ood.make({
                tag: 'p',
                class: 'comment',
                text: comment.text,
              })),
              (text && ood.make({ tag: 'p', class: 'comment', text })),
              controls.el(activityData),
            ],
          }),
        ],
      });
    },
  };
})();
