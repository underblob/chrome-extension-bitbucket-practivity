
const PATH = {
  API_ACTIVITY: 'API_ACTIVITY',
  API_LIKES: 'API_LIKES',
  PULL_REQUEST: 'PULL_REQUEST',
};

const utils = (function() {
  return {
    colorLog(msg, color = 'blue') {
      console.log(`%c chrome-extension-bitbucket-practivity: ${msg}`, `color:${hex[color]}`);
    },

    getPath(key, tokens = {}) {
      const [,, project,, repo,, id] = location.pathname.split('/');
      tokens = { ...tokens, id, project, repo };

      const paths = {
        [PATH.API_ACTIVITY]: '/rest/api/1.0/projects/{project}/repos/{repo}/pull-requests/{id}/activities',
        [PATH.API_LIKES]: '/rest/comment-likes/1.0/projects/{project}/repos/{repo}/pull-requests/{id}/comments/{commentId}/likes',
        [PATH.PULL_REQUEST]: '/projects/{project}/repos/{repo}/pull-requests/{id}{extra}',
      };
      let path = paths[key];

      Object.keys(tokens).forEach(key => {
        const rex = new RegExp(`{${key}}`, 'g');
        path = path.replace(rex, tokens[key]);
      });

      return path;
    },
  };
})();

window.colorLog = utils.colorLog;
