
console.clear();

const EL_ROOT_ID = 'chrome-extension-bitbucket-practivity';

const hex = {
  blue: '#4a6785',
  brown: '#594300',
  green: '#14892c',
  red: '#d04437',
  yellow: '#f6c342',
};

const [,, project,, repo,, id] = location.pathname.split('/');
const PR_KEY = `${project}_${repo}_${id}`;
let PR_STORED;

chrome.storage.sync.get((storage = {}) => {
  colorLog('Extension\'s sync storage:');
  console.log(storage);

  PR_STORED = storage[PR_KEY] || {
    activitiesDeleted: [],
    likesDeleted: [],
  };

  // Make sure USER is defined before first fetch.
  const pollUser = setInterval(() => {
    if (USER && USER.id) {
      clearInterval(pollUser);
      root.fetchData();
    }
  }, 13);
});
