
// Object Oriented DOM

ood = {
  attrs(el, attrs = {}) {
    Object.keys(attrs).forEach(key => {
      const val = attrs[key];
      el.setAttribute(key, val);
    });
  },

  attrVal(el, attr) {
    return el.getAttribute(attr);
  },

  del(selector) {
    const el = ood.el(selector);
    el.parentNode.removeChild(el);
  },

  el(selector, el = document) {
    return el.querySelector(selector);
  },

  els(selector, el = document) {
    const els = el.querySelectorAll(selector);
    return [].slice.call(els);
  },

  html(el, str) {
    el.innerHTML = str;
  },

  make(config = {}) {
    const el = document.createElement(config.tag || 'span');
    delete config.tag;
    const attrs = { ...config };

    const children = attrs.children;
    delete attrs.children;
    if (Array.isArray(children)) {
      children.forEach(child => child && el.appendChild(child));
    }

    const html = attrs.html;
    delete attrs.html;
    if (html) ood.html(el, html);

    const style = attrs.style;
    delete attrs.style;
    if (style) ood.style(el, style);

    const text = attrs.text;
    delete attrs.text;
    if (text) ood.text(el, text);

    const onclick = attrs.onclick;
    delete attrs.onclick;
    if (onclick) ood.on('click', el, onclick);

    ood.attrs(el, attrs);

    return el;
  },

  on(type, el, cb) {
    el.addEventListener(type, cb);
  },

  off(type, el, cb) {
    el.removeEventListener(type, cb);
  },

  style(el, attr = {}) {
    Object.keys(attr).forEach(key => {
      el.style[key] = attr[key];
    });
  },

  text(el, str) {
    el.textContent = str;
  },
};
